package com.b1house.dotaslz.repository;

import com.b1house.dotaslz.dto.MatchQuantityPlayers;
import com.b1house.dotaslz.enums.GameMode;
import com.b1house.dotaslz.model.Match;
import com.b1house.dotaslz.model.Player;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchRepository {
    Match findMatchByIdDota(Player player, String idDota);

    List<Match> findRecentMatchByPlayer(Integer playerId);

    Integer saveMatch(Match match);

    void updateGameMode(Integer matchId, GameMode gameMode);

    List<Match> findAll();

    List<Match> findRecentMatches();

    List<MatchQuantityPlayers> findMultiplePlayersByMatch(Integer seasonId);

    void updateMatchInfos(Integer heroDamage, Integer towerDamage, Integer heroHealing, Integer imp, String award, Integer matchId);

}
