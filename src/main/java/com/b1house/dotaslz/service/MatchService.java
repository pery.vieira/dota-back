package com.b1house.dotaslz.service;


import com.b1house.dotaslz.dto.MatchQuantityPlayers;
import com.b1house.dotaslz.enums.GameMode;
import com.b1house.dotaslz.model.Match;
import com.b1house.dotaslz.model.Player;

import java.util.List;

public interface MatchService {
    Match getMatchByIdDota(Player player, String idDota);

    List<Match> getRecentMatchesByAllPlayers();

    Integer saveMatch(Match match);

    void updateGameMode(Integer matchId, GameMode gameMode);

    List<Match> getAll();

    List<Match> getRecentMatches();

    List<MatchQuantityPlayers> getMultiplePlayersByMatch(Integer seasonId);
    void updateMatchInfos(Integer heroDamage, Integer towerDamage, Integer heroHealing, Integer imp, String award, Integer matchId);
}
