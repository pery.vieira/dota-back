package com.b1house.dotaslz.service.impl;

import com.b1house.dotaslz.model.Player;
import com.b1house.dotaslz.repository.PlayerRepository;
import com.b1house.dotaslz.service.PlayerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {
    private PlayerRepository playerRepository;

    public PlayerServiceImpl(PlayerRepository playerRepository){
        this.playerRepository = playerRepository;
    }
    @Override
    public List<Player> getAllPlayers() {
        return playerRepository.findAllPlayers();
    }

    @Override
    public Player getPlayerById(Integer id) {
        return playerRepository.findPlayerById(id);
    }

    @Override
    public Player getPlayerByIdDota(String idDota) {
        return playerRepository.findPlayerByIdDota(idDota);
    }

    @Override
    public void updateAvatar(Player player) {
        playerRepository.updateAvatar(player);
    }

    @Override
    public void updateNick(Player player) {
        playerRepository.updateNick(player);
    }

    @Override
    public void updateIsPrivate(Player player) {
        playerRepository.updateIsPrivate(player);
    }

    @Override
    public void updateStreak(Player player, Boolean win) {
        playerRepository.updateStreak(player, win);
    }

    @Override
    public Integer getStreakWin(Player player) {
        return playerRepository.getStreakWin(player);
    }

    @Override
    public Integer getStreakLoss(Player player) {
        return playerRepository.getStreakLoss(player);
    }
}
