package com.b1house.dotaslz.ExternalApi;

public class MatchData {
    private MatchInfo match;

    public MatchInfo getMatch() {
        return match;
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
    }
}
