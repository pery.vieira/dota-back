package com.b1house.dotaslz.ExternalApi;

public class MatchInfo {
    private PlayerInfo[] players;

    public PlayerInfo[] getPlayers() {
        return players;
    }

    public void setPlayers(PlayerInfo[] players) {
        this.players = players;
    }
}
