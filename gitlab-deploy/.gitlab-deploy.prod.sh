#!/bin/bash
# Get servers list:
set -f
# Variables from GitLab server:
# Note: They can’t have spaces!!
string=$DEPLOY_SERVER_IP
array=(${string//,/ })
# Iterate servers for deploy and pull last commit
# Careful with the ; https://stackoverflow.com/a/20666248/1057052
for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"
sshpass -p $DEPLOY_SERVER_PASSWD ssh -o StrictHostKeyChecking=no ec2-user@${array[i]} 'cd dota-back git pull origin main docker-compose build docker-compose up -d'
done